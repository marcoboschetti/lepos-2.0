//Biblioteca para Ej10.c
// Va a imprimir hombrecitos y horcas segun la vida (parametro de entrada)
#include "defs.h"

void dibujo(int vida){
        switch(vida)
                {case 7:
                       sys_write_string("        ___________   \n        |         |   \n        |         |   \n        |             \n        |             \n        |             \n        |             \n        |             \n        |             \n      __|__           \n",10);
                        break;
                case 6:
                       sys_write_string( "        ___________   \n        |         |   \n        |         O   \n        |             \n        |             \n        |             \n        |             \n        |             \n        |     Hmmm, sumas una cabeza chiquita \n      __|__           \n",10);
                        break;
                case 5:
                       sys_write_string( "        ___________   \n        |         |   \n        |         O   \n        |         |   \n        |         |   \n        |         |   \n        |             \n        |             \n        |     El flaco este tiene el cuerpo demasiado largo... nacio asi!     \n      __|__           \n",10);
                        break;
                case 4:
                       sys_write_string( "        ___________   \n        |         |   \n        |         O   \n        |         |   \n        |         |          \n        |         |   \n        |        /    \n        |       /     \n        |      /      \n      __|__           \n",10);
                        break;
                case 3:
                       sys_write_string( "        ___________   \n        |         |   \n        |         O   \n        |         |   \n        |         |         \n        |         |   \n        |        / \\ \n        |       /   \\        \n        |      /     \\       \n      __|__           \n",10);
                        break;
                case 2:
                       sys_write_string( "        ___________   \n        |         |   \n        |       \\ O  \n        |        \\|  \n        |         |   \n        |         |   \n        |        / \\ \n        |       /   \\        \n        |      /     \\       \n      __|__           \n",10);
                        break;
                case 1:
                       sys_write_string( "        ___________   \n        |         |   \n        |       \\ O /        \n        |        \\|/       \n        |         |   Hmmmmm casi perdes!     \n        |         |   \n        |        / \\ \n        |       /   \\        \n        |      /     \\       \n      __|__           \n",10);
                        break;
                default:
                        break;
        }
}

#include "defs.h"
#include "sys_calls.h"


#define PLAYERS 2
#define UP 5
#define DOWN 6
#define LEFT 7
#define RIGHT 8
#define NULL 0
#define FOOD 9
#define ENDED 10

//Struct DINAMICO para manejo de la memoria compartida:
//| Buzones de jugadores |    | FEEDER_inbox | ENDED |  MATRIX  |             |
//0					PLAYERS   38             39      40    40+MAP_SIZE	       4Kb

#define MIN_FOOD_TIME 10
#define MAX_FOOD_TIME 20

#define SEM_PREFIX 100+nameSpace
#define MASTER_SEM 98+nameSpace
#define MATRIX_OFFSET 40
#define FEEDER_SEM 99+nameSpace
#define ENDED_GAME_OFFSET 39
#define FEEDER_INBOX 38

#define MIN_COL 0
#define MAX_COL	78
#define MIN_ROW 2
#define MAX_ROW 22
#define MATRIX_ROW MAX_ROW-MIN_ROW
#define MATRIX_COL MAX_COL-MIN_COL

#define MAX_LONG 80*25

#define SHARED_MEM_ID 33+nameSpace

int module(int argc, char argv[5][32]);
int feeder(int argc, char argv[5][32]);
int getNumberOfInstances();

void startGenerics(int argc, char argv[5][32]){

	clear();

	int numberOfInstances=getNumberOfInstances();	
	int nameSpace=numberOfInstances*200;
	sys_set_video_x(2);
	sys_set_video_y(1);
	printf(4+112,"Instancia #%d del Snake",numberOfInstances);

	char * inbox = get_shared_memory(SHARED_MEM_ID);


	for(int m=0;m<2048;m++){
		*(inbox+m)=0;
	}

	*(inbox+ENDED_GAME_OFFSET)=0;

	sem_open(MASTER_SEM);
	
	char p_args[5][32];
	int pids[PLAYERS];

	int i;
	char * p_name="Snake-Player";
	for(i=0;i<PLAYERS;i++){
		inbox[i]=NULL;
		sem_open(SEM_PREFIX+i);
		memcpy(p_args[0],p_name, strlen(p_name)+1);
		p_args[1][0]=i;
		p_args[1][2]=numberOfInstances;
		pids[i]=newTask((TaskFunc)module, 2, p_args);
	}


	srand();
	p_name="Snake-Feeder";
	memcpy(p_args[0],p_name, strlen(p_name)+1);		
	int fed_pid=newTask((TaskFunc)feeder, 2, p_args);
	sem_open(FEEDER_SEM);
	*(inbox+FEEDER_INBOX)=0;

	char * matrix=inbox+MATRIX_OFFSET;
	

	char key_map[][4]={{'w','s','a','d'},{'i','k','j','l'}};

	char ended=0;
	char key;

// Se utiliza un ciclo para determinar la los fps del juego. 
// Podría utilizarse sleep, pero con la implementación realizada, su minima unidad es el segundo.
	int timer=2000;
	int count=0;	

	while(!ended){

		for(count=0;count<timer;count++){
			if((key=sys_read_char())!=0){
				for(i=0;i<PLAYERS;i++){
					if(key==key_map[i][0]){
						inbox[i]=UP;
					}else if(key==key_map[i][1]){
						inbox[i]=DOWN;
					}else if(key== key_map[i][2]){
						inbox[i]=LEFT;
					}else if(key== key_map[i][3]){
						inbox[i]=RIGHT;
					}else{
						inbox[i]=NULL;
					}
				}
			}
		}

		for(i=0;i<PLAYERS;i++){
			sem_up(SEM_PREFIX+i);
			sem_down(MASTER_SEM);
		}
		ended=*(inbox+ENDED_GAME_OFFSET);
		sem_up(FEEDER_SEM);
	}

	sys_set_video_x(1);
	sys_set_video_y(23);

	while((key=sys_read_char())!='\n');

	*(inbox+FEEDER_INBOX)=ENDED;

	sem_up(FEEDER_SEM);

	sem_down(MASTER_SEM);

	
	for(i=0;i<PLAYERS;i++){
		inbox[i]=ENDED;
		sem_up(SEM_PREFIX+i);
		sem_down(MASTER_SEM);
//		sem_close(SEM_PREFIX+i);
	}

//	sem_close(MASTER_SEM);
//	close_shared_memory(SHARED_MEM_ID);
//	sem_close(FEEDER_SEM);
	
	clear();

	return;
}


int module(int argc, char argv[5][32]) {
	
	char numberOfInstances=argv[1][2];	
	int nameSpace=numberOfInstances*200;


	char playerNum=argv[1][0];
	char * inbox=get_shared_memory(SHARED_MEM_ID);
	char * matrix=inbox+MATRIX_OFFSET;
	sem_open(MASTER_SEM);
	int semId=SEM_PREFIX+playerNum;
	sem_open(semId);


	char prevx=playerNum*2+14;
	char prevy=playerNum*2+15;
	char lastMove=0;

	char vecx[MAX_LONG];
	char vecy[MAX_LONG];
	int first_ind=0;
	int last_ind=0;
	vecx[0]=prevx;
	vecy[0]=prevy;
	
	int pending=4;
	int cur_long=1;
	
	char in=NULL;
	while(in!=ENDED){

		sys_set_video_x(0);
		sys_set_video_y(0);

		yield();
		sem_down(semId);
		in=inbox[playerNum];
		
		if((lastMove==DOWN && in == UP) || (lastMove==UP && in == DOWN) || (lastMove==LEFT && in == RIGHT) || (lastMove==RIGHT && in == LEFT)){
			in=NULL;
		} 

		switch(in){
			case ENDED:
			close_shared_memory(SHARED_MEM_ID);
			sem_close(MASTER_SEM);
			sem_close(semId);
			sem_up(MASTER_SEM);
			return;
			break;
			case UP:
			prevy--;
			break;
			case DOWN:
			prevy++;
			break;
			case LEFT:
			prevx-=2;
			break;
			case RIGHT:
			prevx+=2;
			break;
			default:
			switch(lastMove){
				case UP:
				prevy--;
				break;
				case DOWN:
				prevy++;
				break;
				case LEFT:
				prevx-=2;
				break;
				case RIGHT:
				prevx+=2;
				break;
			}
		}

		if(prevx<MIN_COL){
			prevx=MAX_COL;
		}else if(prevx>MAX_COL){
			prevx=MIN_COL;
		}

		if(prevy<MIN_ROW){
			prevy=MAX_ROW;
		}else if(prevy>MAX_ROW){
			prevy=MIN_ROW;
		}

		if((++first_ind)==MAX_LONG){
			first_ind=0;
		}

		if(pending!=0 && lastMove!=0){
			vecx[first_ind]=prevx;
			vecy[first_ind]=prevy;

			cur_long++;
			pending--;
		}else{
			vecx[first_ind]=prevx;
			vecy[first_ind]=prevy;
			
			sys_set_video_y(vecy[last_ind]);
			sys_set_video_x(vecx[last_ind]);	
			sys_write_char(' ',0);
			sys_set_video_x(vecx[last_ind]+1);	
			sys_write_char(' ',0);
			*(matrix+MAX_COL*vecy[last_ind]+vecx[last_ind])=0;

			if((++last_ind)==MAX_LONG){
				last_ind=0;
			}
		}

		sys_set_video_y(vecy[first_ind]);
		sys_set_video_x(vecx[first_ind]);	
		sys_write_char(' ',16*(playerNum+3));
		sys_set_video_x(vecx[first_ind]+1);	
		sys_write_char(' ',16*(playerNum+3));

		switch(*(matrix+MAX_COL*vecy[first_ind]+vecx[first_ind])){
			case FOOD:
			pending++;
			break;
			case 0:
			break;
			default:
			*(inbox+ENDED_GAME_OFFSET)=1;
			break;
		}

		*(matrix+MAX_COL*vecy[first_ind]+vecx[first_ind])=5;

		if(in){
			lastMove=in;
		}

		sem_up(MASTER_SEM);
	}
	clear();

}

int feeder(int argc, char argv[5][32]) {
	char numberOfInstances=argv[1][2];	
	int nameSpace=numberOfInstances*200;

	char * inbox = get_shared_memory(SHARED_MEM_ID);
	char * matrix=inbox+MATRIX_OFFSET;

	sem_open(FEEDER_SEM);
	int cont=4;
	int x,y;
	while(*(inbox+FEEDER_INBOX)!=ENDED){
		sem_down(FEEDER_SEM);
		if((cont--)==0){

			x=randInt(MIN_COL+1,MAX_COL-1);
			y=randInt(MIN_ROW+1,MAX_ROW-1);

			x-=(x%2==0?0:1);

			sys_set_video_y(y);
			sys_set_video_x(x);	
			sys_write_char(' ',16);
			sys_set_video_x(x+1);	
			sys_write_char(' ',16);

			*(matrix+MAX_COL*y+x)=FOOD;

			cont=randInt(MIN_FOOD_TIME,MAX_FOOD_TIME);
		}
	}

	sem_close(FEEDER_SEM);
	close_shared_memory(SHARED_MEM_ID);
	
	clear();
}

int getNumberOfInstances(){
	PrintTask** buffer=getPage();
	list_process(buffer);
	int instanceCount=0;
	int it=0;
	int flag=0;
	PrintTask * tmp;
	while(!flag){
		memcpy(tmp,buffer-((it+1)*sizeof(PrintTask)),sizeof(PrintTask));
		if(tmp->pid==-1){
			flag=1;
		}else{
			if(strcmp("snake",tmp->name)){
				instanceCount++;
			}
		it++;
		}
	}

	freePage(buffer);
	return instanceCount;
}
#ifndef _ints_
#define _ints_

#include <stdint.h>
/*
	Returns a char from the keyboard buffer, 
	or 0 if it's empty
*/


char sys_read_char();

void sys_write_char(char c, char form);

/*
	Requires a null terminated string (0)
*/
void sys_write_string(char * string, char form);

void clear();

unsigned char sys_get_time(char selector);
void sys_set_time(char selector,char value);

//Video ints

int sys_get_video_x();
int sys_get_video_y();

void sys_set_video_x();
void sys_set_video_y();

void sys_video_limit_scroll(int min, int max);

uint64_t * getPage();
void freePage(void * page);

typedef int (*TaskFunc)(void *arg);

struct PrintTask
{
	int pid;
	char * name;
	char * status;
	int waitingPid;
	int remainigTime;
};
typedef struct PrintTask PrintTask;


// Returns -1 as PID of the last element
void list_process(PrintTask ** buffer);
void delete_process(int pid);
void waitPid(int pid);
void sleep(int segs);

void yield();
void toForeground(int pid);

//Asks for an integer as id instead of a String
/*
All sems are initialized in 1 by default. This means they work as a mutex.
It is possible to make several sem_up() to set its value.
*/
void sem_open(int id);
void sem_up(int id);
void sem_down(int id);
void sem_close(int id);

void * get_shared_memory(int id);
void close_shared_memory(int id);

void list_ipcs(PrintTask ** buffer);

typedef struct PrintIpc
{
	char * type;
	int times_opened;
	int value;
	int id;
} PrintIpc;

#endif
#include "sys_calls.h"
#include "defs.h"
#include <stdarg.h>
#define ENTER 	10
#define RANDMAX 32768

static unsigned long next = 1;

void translate_time(unsigned char i, char * buffer);
/*
	The bufferSize should be the lenght of the 
	line required, but the real size of it should be
	lenght+1
*/

void echo(char * str, char fmt){
	new_line();
	new_line();
	sys_write_string(str,fmt);
	new_line();
	new_line();
}

int sys_read_line(char * buffer, int bufferSize){
	char read;
	int i=0;
	while(i<bufferSize-1 && (read=sys_read_char())!=ENTER){
		if(read!=0){
		buffer[i++]=read;
		}
	}
	buffer[i]=0;

	return i;
}

void get_date(char* buffer){
	char * spaces = "  ";
	char aux[30];

	get_weekday(aux);
	concatString(buffer,aux);
	concatString(buffer,spaces);
	get_day(aux);
	concatString(buffer,aux);

	concatString(buffer,"/");
	get_month(aux);

	concatString(buffer,aux);
	concatString(buffer,"/");
	
	get_year(aux);
	concatString(buffer,aux);
	
	return;
}


void get_weekday(char * buffer){
	unsigned char i=sys_get_time(0x06);
	char * aux;
	switch (i){
		case 1:
				aux="Sunday   ";
				break;
		case 2:
				aux="Monday   ";
				break;
		case 3:
				aux="Thursday ";
				break;
		case 4:
				aux="Wednesday";
				break;
		case 5:
				aux="Thursday ";
				break;
		case 6:
				aux="Friday   ";
				break;
		case 7:
				aux="Saturday ";
				break;
	}
	stringCopy(buffer, aux);

	return;	
}

void stringCopy(char * str1, char* str2){
	int i = 0;
	while(str2[i]){
		str1[i]=str2[i];
		i++;
	}
	str1[i]=0;
	return;
}

void print_date(){

	char fmt=113;
	char fmt2=113;
	new_line();
	new_line();
	sys_write_string("------------------------",fmt);
	new_line();
	sys_write_string("|                      |",fmt);
	new_line();
	sys_write_string("|  ",fmt);
	char buffer[30];
	int i=0;
	get_date(buffer);
	while(buffer[i]!=0)
		sys_write_char(buffer[i++],fmt2);
	sys_write_string(" |",fmt);
	new_line();
	sys_write_string("|                      |",fmt);
	new_line();
	sys_write_string("------------------------",fmt);
	new_line();
	new_line();
	return;
}

void get_day(char * buffer){
	unsigned char i=sys_get_time(0x07);
	translate_time(i, buffer);
	return;
}


void get_month(char * buffer){
	unsigned char i=sys_get_time(0x08);
	translate_time(i, buffer);
	return;
}


void get_year(char * buffer){
	unsigned char i=sys_get_time(0x09);
	translate_time(i, buffer);	
	return;
}


/** 
	Gives "HH:MM:SS" as string
**/
void get_time(char* full){
	char aux[10];
	get_hour(aux);
	full[0]=aux[0];
	full[1]=aux[1];
	full[2]=':';

	get_min(aux);
	full[3]=aux[0];
	full[4]=aux[1];
	full[5]=':';

	get_sec(aux);
	full[6]=aux[0];
	full[7]=aux[1];
	full[8]=0;
	return full;
}

void print_time(){
	char fmt=113;
	char fmt2=113;
	char buffer[30];
	get_time(buffer);
	new_line();
	new_line();
	sys_write_string("--------------",fmt);
	new_line();
	sys_write_string("|            |",fmt);
	new_line();
	sys_write_string("|  ",fmt);
	sys_write_string(buffer,fmt2);
	sys_write_string("  |",fmt);
	new_line();
	sys_write_string("|            |",fmt);
	new_line();
	sys_write_string("--------------",fmt);
	new_line();
	new_line();
	return;
}

void print_simple_time(char fmt){
	char buffer[30];
	get_time(buffer);
	sys_write_string(buffer, fmt);
	return;
}

void get_sec(char * buffer){
	unsigned char i=sys_get_time(0x00);
	translate_time(i, buffer);
	return;
}

void get_min(char * buffer){
	unsigned char i=sys_get_time(0x02);
	translate_time(i, buffer);
	return;
}

void get_hour(char * buffer){
	unsigned char i=sys_get_time(0x04);
	translate_time(i, buffer);
	return;
}

void set_time(char * hour, char * min, char * sec){

	sys_set_time(0x04, reverse_translate_time(hour));
	sys_set_time(0x02, reverse_translate_time(min));
	sys_set_time(0x00, reverse_translate_time(sec));
	new_line();
	return;
}

void updateGMT(void){
	char update[30];
	get_hour(update);
	int hour = stringToInt(update);
	intToString((hour-3+24)%24, update);
	set_hour(update);
}



void set_hour(char*hour){
	sys_set_time(0x04, reverse_translate_time(hour));
return;
}


void set_min(char*min){
	sys_set_time(0x02, reverse_translate_time(min));
return;
}


void set_sec(char*sec){
	sys_set_time(0x00, reverse_translate_time(sec));
return;
}

void translate_time(unsigned char i, char * buffer){
	unsigned char aux = i;
	int num = ((i>>4)*10 + (aux & 0x0F));
	if(num<10){
		buffer[0]='0';
		buffer[1]='0'+num;
		buffer[2]=0;
		return;
	}
	intToString(num, buffer);
	return;
}

unsigned char reverse_translate_time(char * str){
 	int start = stringToInt(str);
 	int num=(((start/10)<<4)+start%10);
	
	return num;

}

int stringToInt(char* str){
	int i=0;
	int num=0;
	while (str[i]!=0){
		num = num * 10 + str[i++] - '0';
	}

	return num;

}

void intToString(int number,char* string){
	char index= 0;
	char aux = 0;
	char auxy = 0;
	while(number >=10 ){
		aux = (number % 10);
		string[index] = (aux+'0');
		index++;
		number/= 10;
	}
	string[index++]=number+'0';
	for (int i=0; i<index/2; i++){
		auxy=string[i];
		string[i]=string[index-1-i];
		string[index-1-i]=auxy;
	}


	string[index]=0;
	return;
}

void concatString(char* str1, char* str2){
	int i=0;
	int j=0;
	while( str1[i] != 0){
		i++;
	}
	while(str2[j] != 0){
		str1[i]=str2[j];
		i++;
		j++;
	}
	str1[i]=0;
	return;
}

void appendChar(char*str1,char ch){
	int i=0;
	while( str1[i] != 0){
		i++;
	}

	str1[i++]=ch;
	
	str1[i]=0;


	return;
}



void printf(char style, char * fmt, ...){
	
	va_list argu;
	va_start(argu, fmt);
	char buffer[20];
	int * args = (int *) (&fmt + sizeof(*fmt));
	int i = 0;
	int aux;
	while(fmt[i]){
		if(fmt[i]!='%' && fmt[i]!='\\')
			sys_write_char(fmt[i++], style);
		if(fmt[i]=='\\'){
			if(fmt[++i]){
				sys_write_char(fmt[i++],style);
			}
		}
		if(fmt[i]=='%'){
			if(fmt[++i]){
				switch(fmt[i]){
					case 'd':
						intToString(va_arg(argu, int), buffer);
						sys_write_string(buffer,style);
						break;
					case 's':
						sys_write_string(va_arg(argu,char*), style);
						break;
					case 'c':
						sys_write_char((char)va_arg(argu, int), style);
						break;
					case 'x':
						sys_write_string("0x", 10);
						dec_to_hex(va_arg(argu, int), buffer);
						sys_write_string(buffer, 10);
						break;
					}
					i++;
			}
		}
			
	}
	va_end(argu);
}


int scanf(char * string, char** aux){

	int i = 0, j = 1, flag = 0;
	aux[0] = string;
	while(string[i]){
		if(string[i]==' ' && !flag){
			string[i]=0;
			aux[j++]=string + i + 1;
		}else if(string[i]=='\"'){
			if(!flag)
				aux[j-1]=string + i + 1;
			else
				string[i]=0;
			flag=1-flag;
		}
		i++;
	}
	return j;
}

void new_line(){
	sys_write_char('\n', 80);
}

static char buffer[128] = { 0 };

void dec_to_hex(int value, char * buffer){
	char * p = buffer;
	char *p1, *p2;
	int digits = 0;
	int base = 16;
	do
	{
		int remainder = value % base;
		*p++ = (remainder < 10) ? remainder + '0' : remainder + 'A' - 10;
		digits++;
	}
	while (value /= base);

	// Terminate string in buffer.
	*p = 0;

	//Reverse string in buffer.
	p1 = buffer;
	p2 = p - 1;
	while (p1 < p2)
	{
		char tmp = *p1;
		*p1 = *p2;
		*p2 = tmp;
		p1++;
		p2--;
	}
	return;
}

char toUpper(char c){
	if(c>='a' && c<='z')
		return c-('a'-'A');
	return c;
}

int strlen(char* str){
	int len=0,i=0;
	while(str[i]){
		len++;
		i++;
	}
	return len;
}

int isalpha(char c){
	if(c>='a' && c<='z')
		return 1;
	if(c>='A' && c<='Z')
		return 1;
return 0;
}

char strcmp(char* s1, char*s2){
	int equals=1;
	int i=0;
	while(s1[i]!=0 && s2[i]!=0 && equals){
		if(s1[i]!=s2[i]){
			equals=0;
		}
		i++;
	}
	if(!(s1[i]==0 && s2[i]==0)){
		equals=0;
	}
	return equals;
}


int rand(){
	next = next*1103515245+12345;
	return ((unsigned)(next/65536)%RANDMAX);
}

void srand(){
	int l, acu;
	char aux[20];
	get_time(aux);
	acu = 0;
	l = 0;
	while(buffer[l]){
		acu+=buffer[l++];
	}
	next = acu;
}

int randInt(int min, int max){
	int randNum=rand();
	return rand()%max+min;
}

void printProcess(){

	PrintTask ** buffer=getPage();

	list_process(buffer);

	sys_write_char('\n',0);
	sys_write_string("PID",2);
	sys_write_string("    ",0);
	sys_write_string("NAME",3);
	sys_write_string("    ",0);
	sys_write_string("STATUS",4);
	sys_write_string("    ",0);
	sys_write_string("[WAITED PID]",5);
	sys_write_char('\n',0);

	int it=0;
	int flag=0;
	PrintTask * tmp;
	while(!flag){
		memcpy(tmp,buffer-((it+1)*sizeof(PrintTask)),sizeof(PrintTask));
		if(tmp->pid==-1){
			flag=1;
		}else{
			
			printf(2,"%d",tmp->pid);
			sys_write_string("    ",0);
			sys_write_string(tmp->name,3);
			sys_write_string("    ",0);
			sys_write_string(tmp->status,4);

			if(tmp->waitingPid!=-1){
				sys_write_string("    ",0);
				printf(5,"%d",tmp->waitingPid);
				sys_write_string("    ",0);

			}
		
			sys_write_char('\n',0);

			it++;
		}
	}

	new_line();

	freePage(buffer);
	return;
}

void printIpcs(){
	PrintIpc** buffer=getPage();

	list_ipcs(buffer);

	sys_write_char('\n',0);
	sys_write_string("ID",2);
	sys_write_string("    ",0);
	sys_write_string("TIPO",3);
	sys_write_string("    ",0);
	sys_write_string("APERTURAS",4);
	sys_write_string("    ",0);
	sys_write_string("[VALOR]",5);
	sys_write_char('\n',0);

	int it=0;
	int flag=0;
	PrintIpc * tmp;
	while(!flag){
		memcpy(tmp,buffer-((it+1)*sizeof(PrintTask)),sizeof(PrintTask));
		if(tmp->id==-1){
			flag=1;
		}else{
			
			printf(2,"%d",tmp->id);
			sys_write_string("    ",0);
			sys_write_string(tmp->type,3);
			sys_write_string("    ",0);
			printf(4,"%d",tmp->times_opened);

			if(tmp->value!=-1){
				sys_write_string("    ",0);
				printf(5,"%d",tmp->value);
				sys_write_string("    ",0);

			}
		
			sys_write_char('\n',0);

			it++;
		}
	}

	new_line();

	freePage(buffer);
	return;
}

void sleeper(){
	int sleep_time=60;
	printf(4,"Me duermo. Buenas noches\n");
	sleep(sleep_time);
	printf(4,"Me despierto...\n");
}
#include <stdint.h>
#include "sys_calls.h"
#include "defs.h"

extern char bss;
extern char endOfBinary;

#define STRING 		0
#define INT 		1
#define CHAR 		2
#define FOOTER_SIZE	1
#define HEADER_SIZE	1
#define ROWS		25
#define COLUMNS		80
#define BLOCK		6

#define MAX_PROCESS_ARGS 5
#define MAX_PROCESS_ARG_LEN 32

void * memset(void * destiny, int32_t c, uint64_t length);
int convertArg(char ** args, char * argTypes, int cant);
void intToArgType(int i, char * buffer);
void shell_set_hour(char* hour);
void shell_set_min(char* min);
void shell_set_sec(char* sec);
void _sti();
int test(uint64_t asd);

///////////////////////////////////

void ncPrint(const char * string);
void ncPrintChar(char character);
void ncNewline();
void ncPrintDec(uint64_t value);
void ncPrintHex(uint64_t value);
void ncPrintBin(uint64_t value);
void ncPrintBase(uint64_t value, uint32_t base);
void ncClear();

//////////////////////////////////

struct command
{
	char * name;
	void (* function) ();
	unsigned char * args;
	int argsCant;
	char* desc;
};

static struct command commands[COM] = {};
int snake_instances=0;

static int DEBUG = 0;

int main(int argc, char argv[5][32]) {
	int shell_style=argv[1][0];
	while(1);
	clear();

	clear();
	char p_arg[5][32];

	sys_set_video_y(5);	
	sys_set_video_x(5);	
	updateGMT();

	char buffer[300];
	char lastTime[10];
	char currentTime[10];
	unsigned char c;
	int
	index, 
	flag, 
	i, 
	inputArgs, 
	cont, 
	cursorCont,
	cursorFlag,
	fmt;

	sys_video_limit_scroll(FOOTER_SIZE, ROWS - HEADER_SIZE);

	sys_set_video_y(0);	
	sys_set_video_x(0);	

	for(i=0; i< 80;i++)
		sys_write_char(' ', 112);

	sys_set_video_y(0);	
	sys_set_video_x(30);
	sys_write_string("lep", 127);
	//sys_write_string("OS", shell_style+112);
	sys_write_string(" - ",127);
	//sys_write_string(argv[0],shell_style+112);

	sys_set_video_x(0);
	sys_set_video_y(24);

	for(i=0; i< 80;i++){
		if(i<70)
			sys_write_char(' ', 112);
		else
			;//sys_write_char(' ', shell_style*16);	
	}

	int pos=0;

	get_time(lastTime);
	//updateShellTime(shell_style*16+15);
	sys_set_video_y(1);
	sys_set_video_x(0);
	unsigned char clearArgs[] = {};
	commands[pos].name = "clear";
	commands[pos].function = &clear;
	commands[pos].args = clearArgs;
	commands[pos].argsCant = 0;
	commands[pos].desc = "Clears the screen, restoring it to its purest state";
/*
	unsigned char echoArgs[] = {STRING, CHAR};
	commands[1].name = "echo";
	commands[1].function = &echo;
	commands[1].args = echoArgs;
	commands[1].argsCant = 2;
	commands[1].desc = "Prints a given string with the provided format.";
*/
	pos++;
	unsigned char getTimeArgs[] = {};
	commands[pos].name = "getTime";
	commands[pos].function = &print_time;
	commands[pos].args = getTimeArgs;
	commands[pos].argsCant = 0;
	commands[pos].desc = "Gives the current Time in this format:  HH:MM:SS";
/*
	unsigned char setTimeArgs[] = {STRING,STRING,STRING};
	commands[3].name = "setTime";
	commands[3].function = &set_time;
	commands[3].args = setTimeArgs;
	commands[3].argsCant = 3;
	commands[3].desc = "Sets the system's time to the hours, time and seconds given.";

	unsigned char setSecArgs[] = {STRING};
	commands[4].name = "setSec";
	commands[4].function = &shell_set_sec;
	commands[4].args = setSecArgs;
	commands[4].argsCant = 1;
	commands[4].desc = "Sets de system seconds to the ones provided.";

	unsigned char setMinArgs[] = {STRING};
	commands[5].name = "setMin";
	commands[5].function = &shell_set_min;
	commands[5].args = setMinArgs;
	commands[5].argsCant = 1;
	commands[5].desc = "Sets de system minutes to the ones provided.";

	unsigned char setHourArgs[] = {STRING};
	commands[6].name = "setHour";
	commands[6].function = &shell_set_hour;
	commands[6].args = setHourArgs;
	commands[6].argsCant = 1;
	commands[6].desc = "Sets de system hours to the ones provided.";
*/
	pos++;
	unsigned char printProcessArgs[] = {};
	commands[pos].name = "ps";
	commands[pos].function = &printProcess;
	commands[pos].args = printProcessArgs;
	commands[pos].argsCant = 0;
	commands[pos].desc = "Lists 'em all!";

	pos++;
	unsigned char getDateArgs[] = {};
	commands[pos].name = "getDate";
	commands[pos].function = &print_date;
	commands[pos].args = getDateArgs;
	commands[pos].argsCant = 0;
	commands[pos].desc = "Provides the current date in -Day of the week DD/MM/YY- format.";

	pos++;
	unsigned char helpArgs[] = {};
	commands[pos].name = "help";
	commands[pos].function = &help;
	commands[pos].args = helpArgs;
	commands[pos].argsCant = 0;
	commands[pos].desc = "Gives help on the general use of the system's functions.";

	pos++;
	unsigned char askArgs[] = {};
	commands[pos].name = "ask";
	commands[pos].function = &responde;
	commands[pos].args = askArgs;
	commands[pos].argsCant = 0;
	commands[pos].desc = "Ask something.";

	pos++;
	unsigned char colorArgs[] = {};
	commands[pos].name = "colors";
	commands[pos].function = &colors;
	commands[pos].args = colorArgs;
	commands[pos].argsCant = 0;
	commands[pos].desc = "Wanna see colors?";
	
	pos++;
	unsigned char hangArgs[] = {};
	commands[pos].name = "hangman";
	commands[pos].function = &hangman;
	commands[pos].args = hangArgs;
	commands[pos].argsCant = 0;
	commands[pos].desc = "A classic hangman game";

	pos++;
	unsigned char dropsArgs[] = {};
	commands[pos].name = "colorDrops";
	commands[pos].function = &playColors;
	commands[pos].args = dropsArgs;
	commands[pos].argsCant = 0;
	commands[pos].desc = "Try an aweasome game with colors!";

	pos++;
	unsigned char genArgs[] = {};
	commands[pos].name = "snake";
	commands[pos].function = &startGenerics;
	commands[pos].args = genArgs;
	commands[pos].argsCant = 0;
	commands[pos].desc = "2 players Beta version";

	pos++;
	unsigned char sleeperArgs[] = {};
	commands[pos].name = "sleeper";
	commands[pos].function = &sleeper;
	commands[pos].args = sleeperArgs;
	commands[pos].argsCant = 0;
	commands[pos].desc = "Sleeps to help you check ps and background";

	pos++;
	unsigned char ipcsArgs[] = {};
	commands[pos].name = "ipcs";
	commands[pos].function = &printIpcs;
	commands[pos].args = ipcsArgs;
	commands[pos].argsCant = 0;
	commands[pos].desc = "Lists open sems and sharedMem, if any";

	int * pointer = getPage();
	freePage(pointer);

	while(1){
		i=0;
		flag = 1;
		cont = 200;
		cursorCont = 0;
		cursorFlag = 1;
		//sys_write_string(":)", shell_style);
		sys_write_string(" > ", 15);
		//updateFooter(shell_style*16+15);
		while((c=sys_read_char())!=10){

		get_time(currentTime);
		if(!strcmp(currentTime,lastTime)){
			get_time(lastTime);
			//updateShellTime(shell_style*16+15);
		}

		//sys_write_string(" pagina pedida: ", 12);
		//ncPrintHex(dir);

		if(!cursorCont--){
				cursorCont = 50000;
				if(cursorFlag==1){
					fmt = 0;
					cursorFlag=0;
				}else{
					fmt = 32;
					cursorFlag=1;
				}
				sys_write_char(' ', fmt);
				sys_set_video_x(sys_get_video_x()-1);
			}
			if(c!=0){
				if(c==0x0E){
					if(i){
						i--;
						sys_set_video_x(sys_get_video_x()-1);
						sys_write_string("  ",0);
						sys_set_video_x(sys_get_video_x()-2);
					}
				}
				else{
					buffer[i++] = c;
					sys_write_char(c,10);
				}
				//updateFooter(shell_style*16+15);
			}

		}
		sys_write_char(' ',0);
		
		buffer[i]=0;

		char * args[10];
		inputArgs = scanf(buffer, args);
		strcmp(commands[0].name, args[0]);

		char p_args[MAX_PROCESS_ARGS][MAX_PROCESS_ARG_LEN];


		for(index = 0; flag && index<COM ; index++){
			if(strcmp(commands[index].name, args[0]) && (commands[index].argsCant==inputArgs-1 || (commands[index].argsCant==inputArgs-2  && strcmp(args[inputArgs-1],"&")))){
				flag=0;
				
				int isBackground=strcmp(args[inputArgs-1],"&");
				
				convertArg(args, commands[index].args, commands[index].argsCant);
				
				memcpy(p_args[0], commands[index].name, strlen(commands[index].name)+1);
				int arg=1;
				for(;arg<=commands[index].argsCant;arg++){
					int size=sizeof(int);
					if(commands[index].args[arg-1]==STRING){
						size= strlen(args[arg])+1;
					}
					memcpy(p_args[arg], args[arg],size);
				}

				int sonPid=newTask((TaskFunc)commands[index].function, inputArgs, p_args);
				toForeground(sonPid);
				
				if(!isBackground){
					waitPid(sonPid);

				}
			}
		}

		if(flag){
			if(i!=0){
				sys_write_string("\n______________________________\n\nInvalid command \n______________________________\n", 15);
			}
			sys_write_char('\n',15);
		}
	}
	return 0xDEADBEEF;
}
int test(uint64_t asd){
	ncPrintHex(asd);
	while(1);
}

int convertArg(char ** args, char * argTypes, int cant){
	int  j, flag = 1;
	for(j=1; j<= cant && flag; j++){
		switch(argTypes[j-1]){
			case CHAR:
				args[j]=stringToInt(args[j]);
				break;
			case INT:
				args[j]=stringToInt(args[j]);
				break;
			default:
				break;
		}
	}
}

void * memset(void * destiation, int32_t c, uint64_t length) {
	uint8_t chr = (uint8_t)c;
	char * dst = (char*)destiation;

	while(length--)
		dst[length] = chr;

	return destiation;
}

void help(){
	
	sys_write_string(" en help ", 12);
	int i = 0;
	char * nm = "|->Command name: ";
	char * argC = "Arg count: ";
	char * argT = "  Arg types: ";
	char * spaces = "  ";
	char *dsc = "  |_Description:";
	char buffer[256];
	char c;
	char flag = 1;

	sys_write_string(" antes ", 12);
	
	int * paginaValida = 0x13FFF0FF0 - 0x200000;
	*paginaValida = 4;

	sys_write_string(" despues ", 13);
	sys_write_char(*paginaValida + '0', 15);

	/*
	int * rompedorDePaginas = 0xC0FFEEC0DE;
	printf(12, "print: %d", *rompedorDePaginas = 5);
	ncPrint(" salio del page fault ");
	*/

	while (i < COM && flag){
		clear();
			new_line();
		do{
			new_line();
			sys_write_string(nm, 15);
			sys_write_string(commands[i].name, 11);
			sys_set_video_x(32);
			sys_write_string(argC,15);
			intToString(commands[i].argsCant, buffer);
			sys_write_string(buffer, 11);
			if (commands[i].argsCant)
				sys_write_string(argT,15);
			for(int j=0;j<commands[i].argsCant;j++){
				intToArgType(commands[i].args[j], buffer);
				sys_write_string(buffer, 11);
				sys_write_string(" ",15);
			}
			sys_write_string("  ",15);
			sys_write_char('\n',12);
			sys_write_string(dsc,15);
			sys_write_string(commands[i].desc,12);
			i++;
			new_line();
		}while((i%BLOCK)!=0 && i<COM);
		new_line();
		sys_set_video_y(23);
		printf(10, "For next page type \"N\", to exit type \"Q\".");
		while((c=toUpper(sys_read_char()))!='N' && c!='Q');
		if(c=='Q')
			flag = 0;
	}
	clear();
}

void intToArgType(int i , char * buffer){
	char * tocopy;
	switch(i){
		case 0:
			stringCopy(buffer, "STR");
			break;
		case 1:
			stringCopy(buffer, "INT");
			break;
		case 2:
			stringCopy(buffer, "CHAR");
			break;
	}
	
	return;
}

void colors(){
	new_line();
	char buffer[20];
	for(int i=0; i<16; i++){
		intToString(i, buffer);
		if(i<10 && i)
			sys_write_char(' ',10);
		printf(15, "   %s", buffer);
		//sys_write_string(buffer, 10);
	}
	sys_write_char('\n',12);

	for(int i=0; i<16; i++){
		sys_write_string("   X ",i);
	}

	new_line();
	for(int i=0; i<16*16; i+=16){
		intToString(i, buffer);
		if(i<100)
			sys_write_char(' ',10);
		printf(15,"  %s", buffer);
		//sys_write_string(buffer, 10);
	}
	sys_write_char('\n',12);

	for(int i=0; i<16*16; i+=16){
		sys_write_string("     ", i+1);
	}
	new_line();		
}

void updateShellTime(int fmt){
	int x = sys_get_video_x();
	int y = sys_get_video_y();

	sys_set_video_y(24);	
	sys_set_video_x(71);
	print_simple_time(fmt);

	sys_set_video_x(x);
	sys_set_video_y(y);
}

void updateFooter(int fmt){
	int x = sys_get_video_x();
	int y = sys_get_video_y();

	sys_set_video_y(24);
	sys_set_video_x(10);
	printf(fmt, " X: %d Y: %d  ", x, y);

	sys_set_video_x(x);
	sys_set_video_y(y);
}

void shell_set_hour(char* hour){
	new_line();
	set_hour(hour);
	return;
}
void shell_set_min(char* min){
	new_line();
	set_min(min);
	return;
}
void shell_set_sec(char* sec){
	new_line();
	set_sec(sec);
	return;
}

void memcpy(void * dest, const void * source, int len){
	int i;
	for (i = 0; i < len; i++)
		*((char *)dest + i) = *((char *)source + i);
}

//////////////////////////////////////////////////////////////////


static uint32_t uintToBase(uint64_t value, char * buffer, uint32_t base);


static char buffer[64] = { '0' };
static uint8_t * const video = (uint8_t*)0xB8000;
static uint8_t * currentVideo = (uint8_t*)0xB8000;
static const uint32_t width = 80;
static const uint32_t height = 25 ;

void ncPrint(const char * string)
{
	int i;

	for (i = 0; string[i] != 0; i++)
		ncPrintChar(string[i]);
}

void ncPrintChar(char character)
{
	*currentVideo = character;
	currentVideo += 2;
}

void ncNewline()
{
	do
	{
		ncPrintChar(' ');
	}
	while((uint64_t)(currentVideo - video) % (width * 2) != 0);
}

void ncPrintDec(uint64_t value)
{
	ncPrintBase(value, 10);
}

void ncPrintHex(uint64_t value)
{
	ncPrintBase(value, 16);
}

void ncPrintBin(uint64_t value)
{
	ncPrintBase(value, 2);
}

void ncPrintBase(uint64_t value, uint32_t base)
{
    uintToBase(value, buffer, base);
    ncPrint(buffer);
}

void ncClear()
{
	int i;

	for (i = 0; i < height * width; i++)
		video[i * 2] = ' ';
	currentVideo = video;
}

static uint32_t uintToBase(uint64_t value, char * buffer, uint32_t base)
{
	char *p = buffer;
	char *p1, *p2;
	uint32_t digits = 0;

	//Calculate characters for each digit
	do
	{
		uint32_t remainder = value % base;
		*p++ = (remainder < 10) ? remainder + '0' : remainder + 'A' - 10;
		digits++;
	}
	while (value /= base);

	// Terminate string in buffer.
	*p = 0;

	//Reverse string in buffer.
	p1 = buffer;
	p2 = p - 1;
	while (p1 < p2)
	{
		char tmp = *p1;
		*p1 = *p2;
		*p2 = tmp;
		p1++;
		p2--;
	}

	return digits;
}

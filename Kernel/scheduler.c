#include <stdint.h>
#include "scheduler.h"
#include "allocator.h"
#include "./interrupts.h"
#include "./include/lib.h"
#include <naiveConsole.h>
#include "./paging.h"

#define NULL 0
#define PROCESS_LIST_START 0x710000
#define PROCESS_LIST_END 0x1000000
#define PROCESS_SIZE (sizeof(Task))
#define PROCESS_STACK_START 0x703000
#define MAX_TIME	100000
#define SHELLS_NUM 2


static Task ** processStackPointer = PROCESS_STACK_START;
static int nextPID = 0;
static void * kernelSP;
static Task * current;
static Task * blocked;
static Task * firstBlockedTask;
static Task* terminals[10];
static int current_terminal;
static int elapsedTime = 0;

void initializeTaskStack();
Task * getTask();
void freeTask(void * task);
int validTaskAddress(void * dir);
void nullProcess(int argc, char ** argv);
void * getCurrentStackPointer();
int wrapperFunc(TaskFunc func);
uint64_t * processPushArgs(uint64_t * userStack, int argc, const char argv[5][32]);
void printCurrentRound();
void logShell();

// Sem handler methods
struct Semaphore
{
	int id;
	struct Semaphore * nextSem;
	int timesOpened;
	int value;
	Task * waitingLine;
};
typedef struct Semaphore Semaphore;
Semaphore * semList;


struct SharedMem{
	struct SharedMem * next;
	void * page;
	int id;
	int timesOpened;
};

typedef struct SharedMem SharedMem;
SharedMem * firstSharedMem=0;

void initializeKernelRSP(TaskFunc func){

	initializeTaskStack();
	
	kernelSP=getCurrentStackPointer();
	semList=0;
	current=-1;

	char p_args[5][32];

	mcpy(p_args[0], "Tarea nula", 32);
	current = createProcess((TaskFunc)nullProcess,1,p_args);
	
	current->next = current;
	current->prev = current;

	memcpy(p_args[0], "LOG", 4);


	Task * logTask = createProcess(&logShell, 1, p_args);
	logTask->screenNumber=0;
	terminals[0]=logTask;

//La terminal 0 la usa Kernel como log, las otras no.

	int i;
	for(i=1;i<SHELLS_NUM;i++){
		mcpy(p_args[0], "Shell", 5);
		p_args[0][5]=' ';
		p_args[0][6]=i+'0';
		p_args[0][7]=0;

		p_args[1][0]=(i>=6?i+3:i+1);

		Task * shellTask = createProcess(func, 2, p_args);
		if(i==1)
			shellTask->isForeground=1;


		shellTask->wasInForeground=1;
		shellTask->screenNumber=i;
		terminals[i]=shellTask;
	}

	current_terminal=1;
	firstBlockedTask=NULL;

	logWrite("Tarea Nula creada");

	forceInicialProcess(current->userStack);

}

void * switchUserToKernel(void * esp,int boolean) {
	if(boolean==0){
		current->userStack=esp;
	}else if(boolean==1){
		firstBlockedTask->userStack=esp;
	}else{
		blocked->userStack=esp;
	}


	return kernelSP;
}

void * switchKernelToUser() {

	_cli();
	
	current=current->next;

	if(++elapsedTime > MAX_TIME){
		elapsedTime = 1;
		int auxPid = current->pid;
		do{
			current=current->next;
			if(current->time>MAX_TIME)
				current->time-=MAX_TIME;
			else
				current->time=0;
		}
		while(auxPid!=current->pid);
	}

	if(current->pid || current->next->pid){
		int auxPid = current->prev->pid;
		int taskFlag = 0;
		Task * init = NULL;
		current = current->prev;
		do{
			current=current->next;
			if(!current->pid)
				init = current;
		}
		while(auxPid!=current->pid && (!current->pid || current->time > elapsedTime));
		
		if(auxPid==current->pid && (!current->pid || current->time > elapsedTime)){
			current = init;
		}
	}

//	PDPT[1] = (uint64_t)current->PDT | 7;	
//	refreshCR3();		

	_sti();

	return current->userStack;
}

int KnewTask(TaskFunc func, int argc, uint64_t argv){
	int auxPid= createProcess(func, argc, argv)->pid;
	return auxPid;
}



void wait(int segs){
	current->time = elapsedTime + (int)(segs/0.055);
	int_schedule(0);
}

int getCurrentPid(){
	return current->pid;
}

uint64_t * getNextHeapPage(){
	return current->heapPointer++;
}

uint64_t * processPushArgs(uint64_t * userStack, int argc, const char argv[5][32]){

	int i;
	for (i = argc; i > 0; i--){
		mcpy(userStack - i * 32, argv[argc - i], 32);
	}

	mcpy(userStack - argc * 32 - sizeof(int), &argc, sizeof(int));
	return userStack - argc * 32 - sizeof(int);
}

Task * createProcess(TaskFunc func, int argc, const char argv[5][32]){

	Task * auxTask = getPhysicalPage();
	auxTask->pid = nextPID++;

	ncPrintHex(argv[0]);
	mcpy(auxTask->name, argv[0], 32);

	auxTask->waitedPid=-1;

	
	///////// PAGING ////////
	
	auxTask->PDT = getPhysicalPage();
	memset(auxTask->PDT, 0, PageSize);
	PDPT[auxTask->pid + 1] = (uint64_t)auxTask->PDT | 7;
	uint64_t * PT;
	uint64_t PDT_index, PT_index;
	
	//---------STACK-------//

	PT = getPhysicalPage();
	memset(PT, 0, PageSize);
	
	PDT_index = 511;
	PT_index = 511;

	auxTask->PDT[PDT_index] = (uint64_t)PT | 7;
	PT[PT_index] = (uint64_t)getPhysicalPage() | 7;
	
	//---------HEAP--------//

	PT = getPhysicalPage();
	memset(PT, 0, PageSize);

	PDT_index = 0;
	PT_index = 0;

	auxTask->PDT[PDT_index] = (uint64_t)PT | 7;
	PT[PT_index] = (uint64_t)getPhysicalPage() | 7;

	auxTask->heapPointer = (auxTask->pid + 1) * (uint64_t)0x40000000;

	/////////////////////////

	refreshCR3();

	auxTask->userStack = (auxTask->pid + 1) * (uint64_t)0x40000000 + PDT_index * (uint64_t)0x200000 + PT_index * (uint64_t)0x1000 + 0xFF0;
	auxTask->userStack = processPushArgs(auxTask->userStack, argc, argv);
	auxTask->userStack = createProcessContext(auxTask->userStack, wrapperFunc, func);
	ncPrintHex(auxTask->userStack);
	if(current!=-1){
		Task * aux=current->next;
		current->next=auxTask;
		auxTask->next=aux;

		aux->prev=auxTask;
		auxTask->prev=current;
		auxTask->screenNumber=current->screenNumber;

	}
	auxTask->time=0;
	auxTask->isForeground=0;

	auxTask->wasInForeground=0;


	refreshCR3();


	logWrite("Process created: ");
	logWrite(auxTask->name);
	logWrite("\n");

	return auxTask;
}


int wrapperFunc(TaskFunc func){

	int * argc = (((char *)current->userStack - sizeof (int)) + 0x94);

	char ** argv = ((char **)(current->userStack  + 0x94 ));

	int ans=func(*argc, argv);

	Task * it=firstBlockedTask;
	Task * itprev=firstBlockedTask;
	Task * auxNext = NULL;

	while(it!=NULL){
		if(it->waitedPid==current->pid){
			if(it == itprev){
				firstBlockedTask = it->next;
				if(it->next){
					it->next->prev = firstBlockedTask;
				}
			}else{
				it->prev->next = it->next;
				it->next->prev = it->prev;
			}
			auxNext = it->next;
			it->next=current;
			it->prev = current->prev;
			current->prev->next = it;
			current->prev = it;
			it->waitedPid=-1;
			if(it->wasInForeground){
				it->isForeground=1;
			}
		}else{
			auxNext = it->next;
		}
		it = auxNext;
	}

	uint64_t PML4_indexM, PDPT_indexM, PDT_indexM, PT_indexM;

	PML4_indexM = (uint64_t)current->userStack / (0x8000000000); 
	PDPT_indexM = (uint64_t)current->userStack % (0x8000000000) / (0x40000000); 
	PDT_indexM = (uint64_t)current->userStack % (0x40000000) / (0x200000); 
	PT_indexM = (uint64_t)current->userStack % 0x200000 / 0x1000; 

	uint64_t * currentDir;

	currentDir = PML4[PML4_indexM] & 0xFFFFFFFFFFFFF000;
	currentDir = currentDir[PDPT_indexM] & 0xFFFFFFFFFFFFF000;
	uint64_t * auxDir;

/*
	for(int i = 508; i<512; i++){
		auxDir = currentDir[i] & 0xFFFFFFFFFFFFF000; 
		for(int j = 0; j<512; j++){
			if(auxDir[j]%2){
				freePage(auxDir[j] & 0xFFFFFFFFFFFFF000);
				auxDir[j] = 0;
			}
		}
	}
	refreshCR3();
*/
/*
	freePage(
		(void*)(
			(uint64_t)currentDir[PT_indexM]
			)
		);
*/
	logWrite("Base page: 0x");
	char tmpBuff[15];
	dec_to_hex(current->userStack, tmpBuff);
	logWrite(tmpBuff);
	logWrite("\n");

	ncPrint("free: ");
	ncPrintHex(((uint64_t)current->userStack + 0x3FFF) & 0xFFFF4000);

	current->prev->next = current->next;			
	current->next->prev = current->prev;

	freeTask(current);
	current = current->next;

	forceNext(current->userStack);

	return ans;
}

void deleteProcess(int pid){
	Task * auxTask = current;

	int auxPid = current->pid;
	do{
		current=current->next;
	}
	while(auxPid!=current->pid && current->pid!=pid);
	if(auxPid==current->pid && current->pid!=pid){
		current=firstBlockedTask;
		while(current!=NULL && current->pid!=pid){
			current=current->next;
		}
		if(current==NULL){
			current=auxTask;
			return -1;
		}
	}
	
	Task * it=firstBlockedTask;
	Task * itprev=firstBlockedTask;
	Task * auxNext = NULL;
	while(it!=NULL){
		if(it->waitedPid==current->pid){
			if(it == itprev){
				firstBlockedTask = it->next;
				if(it->next){
					it->next->prev = firstBlockedTask;
				}
			}else{
				it->prev->next = it->next;
				it->next->prev = it->prev;
			}
			auxNext = it->next;
			it->next=current;
			it->prev = current->prev;
			current->prev->next = it;
			current->prev = it;
		}else{
			auxNext = it->next;
		}
		it = auxNext;
	}

	for(int i=0; i<512; i++){
		if(current->PDT[i] & 1){ 
			uint64_t * auxPage = current->PDT[i] & 0xFFFFFFFFFF000;
			for(int j = 0; j< 512; j++){
				if(auxPage[j] & 1){
					freePage(auxPage[j] & 0xFFFFFFFFFF000);
				}
			}
			freePage(current->PDT[i] & 0xFFFFFFFFFF000);
		}
	}
	freePage(current->PDT);

	freePage(
		(void*)(
			((uint64_t)current->userStack + 0x3FFF) & 0xFFFF4000
			)
		);
	
	current->prev->next = current->next;			
	current->next->prev = current->prev;

	freeTask(current);

	if(current==auxTask){
		current=current->next;
		forceNext(current->userStack);
	}else{
		current=auxTask;
	}
	return 1;
}

void waitPid(int pid){
	current->waitedPid=pid;
	current->prev->next = current->next;
	current->next->prev = current->prev;

	Task * auxTask = firstBlockedTask;
	firstBlockedTask = current;
	current = current->next;

	firstBlockedTask->next = auxTask;

	if(auxTask){
		auxTask->prev = firstBlockedTask;
	}

	int_schedule(1);
}



void listProcess(PrintTask ** buffer){
	Task* iterator=current;

	while(iterator->pid!=0){
		iterator=iterator->next;
	}
	
	int index=0;
	char flag=0;
	
	PrintTask * tmp=getPage();
	while(!flag || iterator->pid!=0){
		flag=1;
		
		tmp->pid=iterator->pid;
		tmp->name=iterator->name;
		if(iterator==current){
			tmp->status="Running";
		}else{
			tmp->status="Ready";
		}
		tmp->waitingPid=-1;
		tmp->remainigTime=-1;		

		memcpy(buffer-((index+1)*sizeof(PrintTask)),tmp,sizeof(PrintTask));
		iterator=iterator->next;
		index++;
	}

	iterator=firstBlockedTask;
	while(iterator!=NULL){
		tmp->pid=iterator->pid;
		tmp->name=iterator->name;
		tmp->status="Waiting for process";
		tmp->waitingPid=iterator->waitedPid;
		if(iterator->time==0){
			tmp->remainigTime=-1;
		}else{
			tmp->remainigTime=iterator->time;
		}

		memcpy(buffer-((index+1)*sizeof(PrintTask)),tmp,sizeof(PrintTask));	
		iterator=iterator->next;
		index++;
	}

	Semaphore * semIterator=semList;
	while(semIterator!=NULL){
		iterator=semIterator->waitingLine;
		while(iterator!=NULL){
			tmp->pid=iterator->pid;
			tmp->name=iterator->name;
			tmp->status="Waiting in semaphore";
			tmp->waitingPid=iterator->waitedPid;
			tmp->remainigTime=-1;

			memcpy(buffer-((index+1)*sizeof(PrintTask)),tmp,sizeof(PrintTask));	
			iterator=iterator->next;
			index++;
		}
		semIterator=semIterator->nextSem;
	}

	tmp->pid=-1;
	memcpy(buffer-((index+1)*sizeof(PrintTask)),tmp,sizeof(PrintTask));

	freePage(tmp);
}


/*	char * type;
	int times_opened;
	int value;
	int id;
	PrintIpc;

		struct SharedMem * next;
	void * page;
	int id;
	int timesOpened;
SharedMem * firstSharedMem=0;
*/
void listIpcs(PrintIpc ** buffer){
	Semaphore* iterator=semList;
	
	int index=0;
	PrintIpc * tmp=getPage();
	while(iterator!=0){ 
		tmp->id=iterator->id;
		tmp->value=iterator->value;
		tmp->times_opened=iterator->timesOpened;
		tmp->type="Semaphore";	

		memcpy(buffer-((index+1)*sizeof(PrintTask)),tmp,sizeof(PrintTask));
		iterator=iterator->nextSem;
		index++;
	}
	SharedMem* xiterator=firstSharedMem;
	while(xiterator!=0){ 
		tmp->id=xiterator->id;
		tmp->value=-1;
		tmp->times_opened=xiterator->timesOpened;
		tmp->type="Shared Memory";	

		memcpy(buffer-((index+1)*sizeof(PrintTask)),tmp,sizeof(PrintTask));
		xiterator=xiterator->next;
		index++;
	}


	tmp->id=-1;
	memcpy(buffer-((index+1)*sizeof(PrintTask)),tmp,sizeof(PrintTask));

	freePage(tmp);
}



void initializeTaskStack(){
	int i, j;
	for(i=PROCESS_LIST_START, j=PROCESS_STACK_START;i<PROCESS_LIST_END && j<PROCESS_LIST_START;i+=PROCESS_SIZE, j+=sizeof(void *)){
		freeTask((void *)i);
	}
	ncPrintHex(j);
}

Task * getTask(){
	if(processStackPointer==PROCESS_STACK_START){
		return NULL;
	}	
	processStackPointer--;

	return *(processStackPointer + 1);
}

void freeTask(void * task){
	processStackPointer++;
	*processStackPointer = task;
}

int validTaskAddress(void * dir){
	int valid;

	valid = ((int)(dir - PROCESS_LIST_START) % PROCESS_SIZE == 0) && \
	(dir >= PROCESS_LIST_START) &&
	(dir < PROCESS_LIST_END);

	return valid;
}

void printRound(Task * one){
	Task * auxIt = one;
	int startPid=one->prev->pid;
	ncPrint("[");

		while(auxIt->pid!=startPid){

			ncPrint("(");
				ncPrintDec(auxIt->pid);
				ncPrint(" , ");
				ncPrintHex((int)(auxIt->userStack));
				ncPrint(")");
				ncPrint(" , ");
				auxIt = auxIt->next;
			}
			ncPrint("]");

		}
		void yield(){
			wait(0);
		}

//Kernel code, runs between user tasks
		void kernelChecks(){
			unsigned char make=kernel_give_char();

			if(make!=0){
				int wanted=make-0x3b;

				if(current_terminal!=wanted && wanted>=0 && wanted<SHELLS_NUM){
					setCurrentScreen(wanted);

// Se hace un ciclo para sacar de foreground a los procesos hijos de la terminal vieja y poner a los de la nueva
					Task * iterator=current;
					int startPid=current->pid;
					char flag=0;
					while(!flag || iterator->pid!=startPid){
						flag=1;
						if(iterator->screenNumber==wanted && iterator->wasInForeground){
							iterator->isForeground=1;
						}

						if(iterator->screenNumber==current_terminal){
							iterator->isForeground=0;
						}
						iterator=iterator->next;
					}
					terminals[current_terminal]->isForeground=0;
					current_terminal=wanted;
					if(wanted!=0){
						logWrite("Cambiado a shell ");
						char tmpBuff[5];
						intToString(wanted, tmpBuff);
						logWrite(tmpBuff);
						logWrite("\n");
					}
				}

			}
		}

		char isForeground(){
			return current->isForeground;
		}

		char getCurrentScreenNumber(){
			return current->screenNumber;
		}

		void toForeground(int pid){
			int startPid=current->pid;
			Task* iterator=current;
			char marked=0;
			char flag=0;
			while(!marked || (flag && iterator->pid==startPid)){
				flag=1;
				if(iterator->pid==pid){
					iterator->isForeground=1;
					iterator->wasInForeground=1;
					marked=1;
				}
				iterator=iterator->next;
			}
		}



		void sem_open(int id){
			Semaphore * it=semList;
			char found=0;

			while(it!=0 && !found){

				if(it->id==id){
					found=1;
					it->timesOpened+=1;
					logWrite("Semaphore ");
					char tmpBuff[5];
					intToString(id, tmpBuff);
					logWrite(tmpBuff);
					logWrite(" was already created. Total times opened: ");
					intToString(it->timesOpened, tmpBuff);
					logWrite(tmpBuff);
					logWrite("\n");

				}
				it=it->nextSem;
			}
			if(!found){
				Semaphore * newSem=getPage();
				newSem->nextSem=semList;
				newSem->timesOpened=1;
				newSem->id=id;
				newSem->waitingLine=NULL;
				newSem->value=1;
				semList=newSem;

				char tmpBuff[5];
				logWrite("Semaphore ");
				intToString(id, tmpBuff);
				logWrite(tmpBuff);
				logWrite(" created. \n");

			}
		}


		void sem_up(int id){
			Semaphore * it=semList;
			char flag=0;
			while(it!=0 && !flag){
				if(it->id==id){
					flag=1;
					if(it->value > 0){
						it->value+=1;
					}else{
						if(it->waitingLine!=NULL){
					//There is at least one process waiting in queue. Move to it ready
							Task * toRestore=it->waitingLine;
							it->waitingLine=it->waitingLine->next;


							toRestore->next=current;
							toRestore->prev=current->prev;
							current->prev->next=toRestore;
							current->prev=toRestore;

						}else{
							it->value=1;
						}
					}
				}

				logWrite("Semaphore ");
				char tmpBuff[5];
				intToString(id, tmpBuff);
				logWrite(tmpBuff);
				logWrite(" up. Current value: ");
				intToString(it->value, tmpBuff);
				logWrite(tmpBuff);
				logWrite("\n");
				it=it->nextSem;
			}
		}

		void sem_down(int id){
			Semaphore * it=semList;
			char found=0;

			while(it!=0 && !found){
				if(it->id==id){
					found=1;
					if(it->value > 0){
						it->value-=1;
					}else{
				//If the value was in 0, the process should be blocked till the next up
						current->prev->next=current->next;
						current->next->prev=current->prev;

						Task * nextToRun=current->next;
						current->next=it->waitingLine;
						it->waitingLine=current;

						blocked=current;
						current=nextToRun;
						int_schedule(2);			
					}

				}
				logWrite("Semaphore ");
				char tmpBuff[5];
				intToString(id, tmpBuff);
				logWrite(tmpBuff);
				logWrite(" down. Current value: ");
				intToString(it->value, tmpBuff);
				logWrite(tmpBuff);
				logWrite("\n");
				it=it->nextSem;
			}
		}

		void sem_close(int id){
			Semaphore * prev=0;
			Semaphore * it=semList;
			char found=0;

			while(it!=0 && !found){
				if(it->id==id){
					found=1;
					if((it->timesOpened-=1)==0){
						if(prev==0){
							semList=it->nextSem;
						}else{
							prev->nextSem=it->nextSem;
						}
						logWrite("Deleting semaphore ");
						char tmpBuff[5];
						intToString(id, tmpBuff);
						logWrite(tmpBuff);
						logWrite("\n");

					
						freePage(it);
					}else{
						logWrite("Semaphore ");
						char tmpBuff[5];
						intToString(id, tmpBuff);
						logWrite(tmpBuff);
						logWrite(" is still opened. Total times opened: ");
						intToString(it->timesOpened, tmpBuff);
						logWrite(tmpBuff);
						logWrite("\n");
					}
				}
				prev=it;
				it=it->nextSem;
			}
		}

		void logShell(){
			int i;
			clearScreen();
			setY(0);	
			setX(0);
			for(i=0; i< 80;i++)
				sys_write_char(' ', 112);

			setY(0);	
			setX(35);
			sys_write_string("LOG", 127);
			setX(0);
			setY(1);
			while(1);
		}

		void logWrite(char * str){
			int lastScreen=current->screenNumber;
			current->screenNumber=0;
			sys_write_string(str,15);
			current->screenNumber=lastScreen;
			return;
		}

		void intToString(int number,char* string){
			char index= 0;
			char aux = 0;
			char auxy = 0;
			while(number >=10 ){
				aux = (number % 10);
				string[index] = (aux+'0');
				index++;
				number/= 10;
			}
			string[index++]=number+'0';
			for (int i=0; i<index/2; i++){
				auxy=string[i];
				string[i]=string[index-1-i];
				string[index-1-i]=auxy;
			}

			string[index]=0;
			return;
		}



void * get_shared_memory(int id){
	SharedMem * it=firstSharedMem;
	char found=0;
	void * page;
	while(it!=0 && !found){
		if(it->id==id){
			found=1;
			page=it->page;
			it->timesOpened+=1;
			logWrite("Shared Memory  ");
			char tmpBuff[5];
			intToString(id, tmpBuff);
			logWrite(tmpBuff);
			logWrite(" was already created. Total times opened: ");
			intToString(it->timesOpened, tmpBuff);
			logWrite(tmpBuff);
			logWrite("\n");
		}else{
			it=it->next;
		}
	}
	if(!found){
		SharedMem * newShared=getPage();
		newShared->next=firstSharedMem;
		firstSharedMem=newShared;
		newShared->timesOpened=1;
		newShared->id=id;
		newShared->page=getPage();
		page=newShared->page;

		char tmpBuff[5];
		logWrite("Shared Memory  ");
		intToString(id, tmpBuff);
		logWrite(tmpBuff);
		logWrite(" created. \n");
	}
	return page;
}

void close_shared_memory(int id){

	SharedMem * prev=0;
	SharedMem * it=firstSharedMem;
	char found=0;
	
	while(it!=0 && !found){
		if(it->id==id){
			found=1;
			if((it->timesOpened-=1)==0){
				if(prev==0){
					firstSharedMem=it->next;
				}else{
					prev->next=it->next;
				}

				logWrite("Deleting Shared Memory  ");
				char tmpBuff[5];
				intToString(id, tmpBuff);
				logWrite(tmpBuff);
				logWrite("\n");
				freePage(it->page);
				freePage(it);
			}else{
				logWrite("Shared Memory ");
				char tmpBuff[5];
				intToString(id, tmpBuff);
				logWrite(tmpBuff);
				logWrite(" is still opened. Total times opened: ");
				intToString(it->timesOpened, tmpBuff);
				logWrite(tmpBuff);
				logWrite("\n");
			}
		}
		prev=it;
		it=it->next;
	}

}

#include "./allocator.h"
#include "./paging.h"
#include "./scheduler.h"

#define NULL 0
#define HEAP_START 0x2000000
#define HEAP_END 0x40000000
#define PAGE_SIZE 0x1000
#define STACK_START 0x1000000

static void** pageStackPointer = STACK_START;
static initialized;
static int DEBUG = 0;	

void initializePageStack(){
	int i;
	initialized=0;
	for(i=HEAP_START;i<HEAP_END;i+=PAGE_SIZE){
		freePhysicalPage((void*)i);
	}
	initialized=1;
}

uint64_t * getPhysicalPage(){
	if(pageStackPointer==STACK_START)
		return NULL;
	pageStackPointer--;

	logWrite("Allocated page: 0x");
	char tmpBuff[15];
	dec_to_hex(*(pageStackPointer + 1), tmpBuff);
	logWrite(tmpBuff);
	logWrite("\n");

//	ncPrint(":NP:");
//	ncPrintHex(*(pageStackPointer + 1));

	return *(pageStackPointer + 1);
}


uint64_t * getPage(){
	ncClear();
	uint64_t PDPT_indexM, PDT_indexM, PT_indexM, * currentPDT, * currentPT;
	uint64_t * virtual;
	virtual = getNextHeapPage();
	PDPT_indexM = (uint64_t)virtual % (0x8000000000) / (0x40000000); 
	PDT_indexM = (uint64_t)virtual % (0x40000000) / (0x200000); 
	PT_indexM = (uint64_t)virtual % 0x200000 / 0x1000; 

	/*ncPrint(" PDPT: ");
	ncPrintDec(PDPT_indexM);
	ncPrint("======");

	ncPrint(" PDT: ");
	ncPrintDec(PDT_indexM);
	ncPrint("======");
	
	ncPrint(" PT: ");
	ncPrintDec(PT_indexM);
	ncPrint("======");*/
	

	currentPDT = PDPT[getCurrentPid()+1] && 0xFFFFFFFFFFFFF000;
	if(!(currentPDT[PDT_indexM]%2)){
		currentPT = (uint64_t) getPhysicalPage();
		memset(currentPT,0,0x1000); 
		currentPDT[PDT_indexM] = (uint64_t)currentPT | 7;
	}else{
		currentPT = currentPDT[PDT_indexM] & 0xFFFFFFFFFFFFF000;
	}
	currentPT[PT_indexM] = (uint64_t)getPhysicalPage() | 7;

	refreshCR3();

	return virtual;
}

void freePage(void* virtual){
	uint64_t PDPT_indexM, PDT_indexM, PT_indexM, *currentPDPT, * currentPDT, * currentPT;

	PDPT_indexM = (uint64_t)virtual % (0x8000000000) / (0x40000000); 
	PDT_indexM = (uint64_t)virtual % (0x40000000) / (0x200000); 
	PT_indexM = (uint64_t)virtual % 0x200000 / 0x1000;
	ncClear();
	ncPrint("PDPT:");
	ncPrintDec(PDPT_indexM);
	ncPrint(":PDT:");
	ncPrintDec(PDT_indexM);
	ncPrint(":PT:");
	ncPrintDec(PT_indexM);
	ncPrint(".-");
	currentPDT = (PDPT[PDPT_indexM]) & 0xFFFFFFFFFFFFF000;
	currentPT = (currentPDT[PDT_indexM]) & 0xFFFFFFFFFFFFF000;
	freePhysicalPage((currentPT[PT_indexM]) & 0xFFFFFFFFFFFFF000);

	refreshCR3();
	//while(1);
}

void freePhysicalPage(void * page){

	pageStackPointer++;
	*pageStackPointer = page;

	if(initialized){

		logWrite("Free page: 0x");
		char tmpBuff[15];
		dec_to_hex(page,tmpBuff);
		logWrite(tmpBuff);
		logWrite("\n");
	}
}

int validAddress(void * dir){
	int valid;

	valid = ((int)(dir - HEAP_START) % PAGE_SIZE == 0) && \
			(dir >= HEAP_START) &&
			(dir < HEAP_END);

	return valid;
}

void dec_to_hex(int value, char * buffer){
	char * p = buffer;
	char *p1, *p2;
	int digits = 0;
	int base = 16;
	do
	{
		int remainder = value % base;
		*p++ = (remainder < 10) ? remainder + '0' : remainder + 'A' - 10;
		digits++;
	}
	while (value /= base);

	// Terminate string in buffer.
	*p = 0;

	//Reverse string in buffer.
	p1 = buffer;
	p2 = p - 1;
	while (p1 < p2)
	{
		char tmp = *p1;
		*p1 = *p2;
		*p2 = tmp;
		p1++;
		p2--;
	}
	return;
}
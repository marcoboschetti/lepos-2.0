#include <stdint.h>
#include <string.h>
#include <lib.h>
#include "types.h"
#include "defs.h"
#include <moduleLoader.h>
#include <naiveConsole.h>
#include "interrupts.h"
#include "allocator.h"
#include "scheduler.h"

extern uint8_t text;
extern uint8_t rodata;
extern uint8_t data;
extern uint8_t bss;
extern uint8_t endOfKernelBinary;
extern uint8_t endOfKernel;

static const uint64_t PageSize = 0x1000;

static void * const sampleCodeModuleAddress = (void*)0x400000;
static void * const sampleDataModuleAddress = (void*)0x500000;

IDTEntry  * idt = (IDTEntry*) 0x0;

typedef int (*EntryPoint)();


void clearBSS(void * bssAddress, uint64_t bssSize)
{
	memset(bssAddress, 0, bssSize);
}

void * getStackBase()
{
	return (void*)(
		(uint64_t)&endOfKernel
		+ PageSize * 8				//The size of the stack itself, 32KiB
		- sizeof(uint64_t)			//Begin at the top of the stack
	);
}

void * initializeKernelBinary()
{	
	char buffer[10];
	
	ncPrint("[x64BareBones]");
	ncNewline();

	ncPrint("CPU Vendor:");
	ncPrint(cpuVendor(buffer));
	ncNewline();

	ncPrint("[Loading modules]");
	ncNewline();
	void * moduleAddresses[] = {
		sampleCodeModuleAddress,
		sampleDataModuleAddress
	};
	
	loadModules(&endOfKernelBinary, moduleAddresses);
	ncPrint("[Done]");
	ncNewline();
	ncNewline();

	ncPrint("[Initializing kernel's binary]");
	ncNewline();

	clearBSS(&bss, &endOfKernel - &bss);


	ncPrint("  text: 0x");
	ncPrintHex((uint64_t)&text);
	ncNewline();
	ncPrint("  rodata: 0x");
	ncPrintHex((uint64_t)&rodata);
	ncNewline();
	ncPrint("  data: 0x");
	ncPrintHex((uint64_t)&data);
	ncNewline();
	ncPrint("  bss: 0x");
	ncPrintHex((uint64_t)&bss);
	ncNewline();

	ncPrint("[Done]");
	ncNewline();
	ncNewline();
	
	return getStackBase();
}

int main()
{	
	_cli();
	ncPrint("[Kernel Main]");
	ncNewline();

    setupIDTEntry (0x0E, 0x08, (uint64_t)&_pageFaultHandler, ACS_TRAP);
 	setupIDTEntry (0x20, 0x08, (uint64_t)&_irq00Handler, ACS_INT);
    setupIDTEntry (0x21, 0x08, (uint64_t)&_irq01Handler, ACS_INT);
    setupIDTEntry (0x80, 0x08, (uint64_t)&_sysCallHandler, ACS_TRAP);
    //memset(0xA00000, 0, 0x40000000 - 0xA00000);
	
	initializePaging();
    initialize_keyboard();
    picMasterMask(0xFC);
    picSlaveMask(0xFF);

	ncPrint("  Sample code module at 0x");
	ncPrintHex((uint64_t)sampleCodeModuleAddress);
	ncNewline();

	ncPrint("Initializing page stack");
	initializePageStack();

	ncPrint("  Calling the sample code module returned: ");

	clearScreen();	
	
	initializeKernelRSP((TaskFunc)sampleCodeModuleAddress);
	
	ncNewline();
	ncNewline();

	ncPrint("  Sample data module at 0x");
	ncPrintHex((uint64_t)sampleDataModuleAddress);
	ncNewline();
	ncPrint("  Sample data module contents: ");
	ncPrint((char*)sampleDataModuleAddress);
	ncNewline();

	ncPrint("[Finished]");
	return 0;
}

void setupIDTEntry (int index, uint16_t selector, uint64_t offset, uint8_t type_attr) {
   idt[index].offsetLow = offset & 0xFFFF;
   offset>>=16;
   idt[index].selector = selector;
   idt[index].zero1 = 0;
   idt[index].type_attr = type_attr;
   idt[index].offsetMid =offset & 0xFFFF;
   offset>>=16;
   idt[index].offsetHigh = offset;
   idt[index].zero2 = 0;
   idt[index].zero3 = 0;
   idt[index].zero4 = 0;
   idt[index].zero5 = 0;  
}  


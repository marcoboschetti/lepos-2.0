#include <stdint.h>
#include "scheduler.h"
#include "allocator.h"
#include "./interrupts.h"
#include "paging.h"

void setPML4(long);
void setPDPE(long);
void setPDE(long, long);

/*
static uint64_t * PML4 = (uint64_t*) 	0x00700000;
static uint64_t * PDPE = (uint64_t*) 	0x00701000;
static uint64_t * PDE = (uint64_t*) 	0x00702000;
*/

void initializePaging(){

	int pageDirectoryAddr = PDT, pageAddr = 0x000000;
	memset(PML4, 0,  PageSize * 6);
	PML4[0] = (uint64_t)PDPT | 7;
	PDPT[0] = 0x87;
	setCR3();

}

void pageFaultHandler(uint64_t errorCode, uint64_t page){
	ncPrint("error:");
	ncPrintHex(errorCode);
	ncPrint("|page:");
	ncPrintHex(page);

	ncPrint(" pid: ");
	int pid = getCurrentPid();
	ncPrintDec(pid);

	uint64_t PML4_indexM, PDPT_indexM, PDT_indexM, PT_indexM;
	uint64_t *currentPT, * currentPDT;;
	
	PML4_indexM = (uint64_t)page / (0x8000000000); 
	PDPT_indexM = (uint64_t)page % (0x8000000000) / (0x40000000); 
	PDT_indexM = (uint64_t)page % (0x40000000) / (0x200000); 
	PT_indexM = (uint64_t)page % 0x200000 / 0x1000; 

//	currentDir = PML4[PML4_indexM] & 0xFFFFFFFFFFFFF000;
//	currentDir = currentDir[PDPT_indexM] & 0xFFFFFFFFFFFFF000;

	ncPrint(" PDPT index: ");
	ncPrintDec(PDPT_indexM);
	ncPrint(" PDT index: ");
	ncPrintDec(PDT_indexM);
	ncPrint(" PT index: ");
	ncPrintDec(PT_indexM);
	
	if(PDPT_indexM==(pid+1)){
		/*
		currentDir = (uint64_t)PML4[0] & 0xFFFFFFFFFFFFF000;
		currentDir[PDPT_indexM] = 0x0 | 0x87;
		*/

		if(PDT_indexM>507){
			currentPDT = PDPT[PDPT_indexM] & 0xFFFFFFFFFFFFF000;
			if(!(currentPDT[PDT_indexM]%2)){
				ncPrint(" faltaba el par de megas ");
				currentPDT[PDT_indexM] = (uint64_t)getPhysicalPage() | 7;
			}
			ncPrint(" pagina asignada ");
			currentPT = currentPDT[PDT_indexM] & 0xFFFFFFFFFFFFF000;
			currentPT[PT_indexM] = (uint64_t)getPhysicalPage() | 7;
		}else{
			ncPrint(" mal mega ");
			while(1);
		}
	}else{
		ncPrint(" mal giga");
		while(1);
	}
	
	ncPrint(" refreshing ");
	refreshCR3();
	ncPrint(" refreshed ");
	return;
}


#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <stdint.h>


struct Task
{
	int pid;
	uint64_t * userStack;
	char name[32];
	struct Task * next;
	struct Task * prev;
	char * isForeground;
	int waitedPid;
	int time;
	char screenNumber;
	char wasInForeground;

	uint64_t * PDT;
	uint64_t * currentPage;
	uint64_t * heapPointer;

};

typedef struct Task Task;

typedef int (*TaskFunc)(int argc, char ** argv);

typedef struct PrintTask
{
	int pid;
	char * name;
	char * status;
	int waitingPid;
	int remainigTime;
} PrintTask;


Task * createProcess(TaskFunc func, int argc, const char argv[5][32]);
Task * createProcessDEBUG(TaskFunc func, int argc, const char argv[5][32]);

int getCurrentPid();
void listProcess(PrintTask ** buffer);
void deleteProcess(int pid);
uint64_t * getNextHeapPage();

uint64_t * createProcessContext(void * userStack, TaskFunc wrapper,TaskFunc func );
void *  getCurrentStackPointer();

int KnewTask(TaskFunc func, int argc, uint64_t argv);

void wait(int segs);

void * switchUserToKernel(void * esp, int boolean);
void * switchKernelToUser();
void * forceInicialProcess(void * esp);
void forceNext(void * esp);

void int_schedule(int boolean);

void yield();
void kernelCheks();
char isForeground();

void toForeground(int pid);
char getCurrentScreenNumber();

void sem_open(int id);
void sem_up(int id);
void sem_down(int id);
void sem_close(int id);

void logWrite(char * str);

typedef struct PrintIpc
{
	char * type;
	int times_opened;
	int value;
	int id;
} PrintIpc;

void listIpcs(PrintIpc ** buffer);

#endif


/*

#mapa  #memoria

NODOS-task



------------ 512MB
	Pages

------------ 7MB
	ALLOC

------------ 6MB
	UserData

------------ 5MB
	UserCod

----------- 4MB
	SCHEDULER

	KERNEL


NA | NB | NC



NA-> NB-> NC->
*/
#define ROWS 25
#define COLUMNS 80
#define VIDEO_ADDR (char *) 0xB8000;
#define BACKSPACE 0x0E
#define ENTER 10


char *real_screen = VIDEO_ADDR;


static int minScroll=0;
static int maxScroll=25;

static int xarr[10]={0};
static int yarr[10]={0};

static char screens[10][80*25*2]={0};

static char current_screen=1;

void sys_write_char(char character,char form);
void sys_write_string(char* string,char form);
void sys_write_scroll();
void setCurrentScreen(char i);


char *screen = VIDEO_ADDR;

void setCurrentScreen(char i){
	current_screen=i;
	memcpy(real_screen,screens[i],sizeof(char)*80*25*2);
}


void sys_write_char(char character,char form){

	int screenNmbr=getCurrentScreenNumber();

	if(character==BACKSPACE){
		if(xarr[screenNmbr]==0){
			if(yarr[screenNmbr]!=minScroll){
				yarr[screenNmbr]--;
				xarr[screenNmbr]=COLUMNS-1;
			}
		}
		else{
			xarr[screenNmbr]--;
		}	
		screens[screenNmbr][(yarr[screenNmbr]*COLUMNS + xarr[screenNmbr])*2] = ' ';
		if(current_screen==screenNmbr){
			real_screen[(yarr[screenNmbr]*COLUMNS + xarr[screenNmbr])*2] = ' ';
		}
	}else if(character==ENTER){
		if(yarr[screenNmbr]==maxScroll - 1){
			sys_write_scroll();
		}
		yarr[screenNmbr]++;
		xarr[screenNmbr]=0;
	}else{
		screens[screenNmbr][(yarr[screenNmbr]*COLUMNS + xarr[screenNmbr])*2] = character;
		screens[screenNmbr][(yarr[screenNmbr]*COLUMNS + xarr[screenNmbr])*2+1] = form;
		if(current_screen==screenNmbr){
			real_screen[(yarr[screenNmbr]*COLUMNS + xarr[screenNmbr])*2] = character;
			real_screen[(yarr[screenNmbr]*COLUMNS + xarr[screenNmbr])*2+1] = form;
		}
		xarr[screenNmbr]++;
		if(xarr[screenNmbr]==COLUMNS){
			yarr[screenNmbr]++;
			xarr[screenNmbr]=0;
			if(yarr[screenNmbr]==maxScroll){
				sys_write_scroll();
			}
		}
	}
	return;
}

void sys_write_string(char * string, char form){
	int i=0;
	for (i = 0; string[i] != 0; i++)
		sys_write_char(string[i],form);
}

void sys_write_scroll(){
	int screenNmbr=getCurrentScreenNumber();

	int i,j;
	for(i=0;i<COLUMNS;i++){
		for(j=minScroll + 1;j<maxScroll;j++){
			screens[screenNmbr][((j-1)*COLUMNS + i)*2] = screens[screenNmbr][(j*COLUMNS + i)*2] ;
			screens[screenNmbr][((j-1)*COLUMNS + i)*2+1] = screens[screenNmbr][(j*COLUMNS + i)*2+1];
			if(screenNmbr==current_screen){
				real_screen[((j-1)*COLUMNS + i)*2] = screens[screenNmbr][(j*COLUMNS + i)*2] ;
				real_screen[((j-1)*COLUMNS + i)*2+1] = screens[screenNmbr][(j*COLUMNS + i)*2+1];
			}

		}
	}

	for(i=0;i<COLUMNS;i++){
		screens[screenNmbr][((j-1)*COLUMNS + i)*2] = 0 ;
		screens[screenNmbr][((j-1)*COLUMNS + i)*2+1] = 0;
		if(screenNmbr==current_screen){
			real_screen[((j-1)*COLUMNS + i)*2] = 0;
			real_screen[((j-1)*COLUMNS + i)*2+1] = 0;
		}

	}

	if(yarr[screenNmbr]!=0){
		yarr[screenNmbr]--;
	}
}


void clearScreen(){
	int screenNmbr=getCurrentScreenNumber();

	int i;
	for(i=(minScroll+1)*COLUMNS;i<(maxScroll)*COLUMNS*2;i++){
		screens[screenNmbr][i]=3;
	}
	if(screenNmbr==current_screen){
		for(i=(minScroll+1)*COLUMNS;i<(maxScroll)*COLUMNS*2;i++){
			real_screen[i]=3;
		}
	}

	xarr[screenNmbr]=0;
	yarr[screenNmbr]=minScroll;
}

int getX(){
	int screenNmbr=getCurrentScreenNumber();

	return xarr[screenNmbr];
}

void setX(int i){
	int screenNmbr=getCurrentScreenNumber();

	xarr[screenNmbr]=i;
}

int getY(){
	int screenNmbr=getCurrentScreenNumber();

	return yarr[screenNmbr];
}

void setY(int j){
	int screenNmbr=getCurrentScreenNumber();

	yarr[screenNmbr]=j;
}

void set_video_scrollLimit(int min, int max){
	minScroll=min;
	maxScroll=max;
	return;
}